﻿namespace RegistroAdministraccion
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.cATALOGOSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tIPOPERSONALToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pERSONALToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cHECADASToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(158)))), ((int)(((byte)(148)))));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cATALOGOSToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(756, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // cATALOGOSToolStripMenuItem
            // 
            this.cATALOGOSToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tIPOPERSONALToolStripMenuItem,
            this.pERSONALToolStripMenuItem,
            this.cHECADASToolStripMenuItem});
            this.cATALOGOSToolStripMenuItem.Name = "cATALOGOSToolStripMenuItem";
            this.cATALOGOSToolStripMenuItem.Size = new System.Drawing.Size(85, 20);
            this.cATALOGOSToolStripMenuItem.Text = "CATALOGOS";
            // 
            // tIPOPERSONALToolStripMenuItem
            // 
            this.tIPOPERSONALToolStripMenuItem.Name = "tIPOPERSONALToolStripMenuItem";
            this.tIPOPERSONALToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.tIPOPERSONALToolStripMenuItem.Text = "TIPO PERSONAL";
            this.tIPOPERSONALToolStripMenuItem.Click += new System.EventHandler(this.tIPOPERSONALToolStripMenuItem_Click);
            // 
            // pERSONALToolStripMenuItem
            // 
            this.pERSONALToolStripMenuItem.Name = "pERSONALToolStripMenuItem";
            this.pERSONALToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.pERSONALToolStripMenuItem.Text = "PERSONAL";
            this.pERSONALToolStripMenuItem.Click += new System.EventHandler(this.pERSONALToolStripMenuItem_Click);
            // 
            // cHECADASToolStripMenuItem
            // 
            this.cHECADASToolStripMenuItem.Name = "cHECADASToolStripMenuItem";
            this.cHECADASToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.cHECADASToolStripMenuItem.Text = "CHECADAS";
            this.cHECADASToolStripMenuItem.Click += new System.EventHandler(this.cHECADASToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.ClientSize = new System.Drawing.Size(756, 386);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Administraccion";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem cATALOGOSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tIPOPERSONALToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pERSONALToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cHECADASToolStripMenuItem;
    }
}

