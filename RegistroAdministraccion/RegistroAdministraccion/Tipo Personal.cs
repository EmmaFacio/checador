﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaNegocio.RegistroAdministraccion;
using Entidades.RegistroAdministraccion;

namespace RegistroAdministraccion
{
    public partial class Tipo_Personal : Form
    {
        private MDatos _mDatos;
        private BPersonal _bPersonal;
        private bool _isEnableBinding = false;

        public Tipo_Personal()//constructor de la vista
        {
            InitializeComponent();
            _mDatos = new MDatos();
            _bPersonal = new BPersonal();
            _isEnableBinding = true;
            BindingBPersonal(); //Actualiza datos 
        }
        public Tipo_Personal(BPersonal bPersonal)
        {
            //Se esta haciendo otro constructor para mandar llamar los datos que se estan
            //agregando y guardando los mande al datagrid automaticamente

            _mDatos = new MDatos();
            _bPersonal = new BPersonal();

        }

        private void Tipo_Personal_Load(object sender, EventArgs e)
        {
            buscarTipoPersonal("");
        }
        private void BindingBPersonal()
        {
            if (_isEnableBinding)
            {
                
               // _bPersonal.RFC = textBox1.Text;
                _bPersonal.Nombre = textBox2.Text;
                _bPersonal.ApellidoP = textBox3.Text;
                _bPersonal.ApellidoM = textBox4.Text;
                _bPersonal.FechaNacimeinto = textBox5.Text;


            }

        }

        private void buscarTipoPersonal(string filtro)
        {
            Dtg_Datoss.DataSource = _mDatos.ObtenerLista(filtro);
        }

        private void Tipo_de_Personal_Load(object sender, EventArgs e)
        {
            buscarTipoPersonal("");
        }

        private void btn_Nuevo_Click(object sender, EventArgs e)
        {
            BindingBPersonal();
            Guardar();
            MessageBox.Show("Registrado correctamente!!");
            this.Close();
            buscarTipoPersonal("");


           
        }

        private void Guardar()

        {
            _mDatos.Guardar(_bPersonal);


        }

        private void btn_Eliminar_Click(object sender, EventArgs e)
        {

            if (MessageBox.Show("estas seguro que deseas eliminar este registro?", "Eliminar registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    Eliminar();
                    buscarTipoPersonal("");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void Eliminar()

        {
            _mDatos.Eliminar(_bPersonal);


        }

         private void btn_Actualizar_Click(object sender, EventArgs e)
         {

             BindingBPersonal();
             buscarTipoPersonal("");



             Actualizar();
         }
         private void Actualizar()

         {
             _mDatos.Actualizar(_bPersonal);


         }
     }



    }

