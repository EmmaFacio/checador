﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RegistroAdministraccion
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void tIPOPERSONALToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Tipo_de_Personal pero = new Tipo_de_Personal();
            pero.ShowDialog();
        }

        private void pERSONALToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Tipo_Personal per = new Tipo_Personal();
            per.ShowDialog();
        }

        private void cHECADASToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Checador pe = new Checador();
            pe.ShowDialog();
        }
    }
}
