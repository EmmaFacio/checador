﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaNegocio.RegistroAdministraccion;
using Entidades.RegistroAdministraccion;

namespace RegistroAdministraccion
{
    public partial class Tipo_de_Personal : Form
    {

        private LPersona _lpersona;
        private ETipoPersonal _etipopersonal;
        private bool _isEnableBinding = false;

        public Tipo_de_Personal()//constructor de la vista
        {
            InitializeComponent();
            _lpersona = new LPersona();
            _etipopersonal = new ETipoPersonal();
            _isEnableBinding = true;
            BindingEtipoPersonal(); //Actualiza datos 
        }
        public Tipo_de_Personal(ETipoPersonal eTipoPersonal)
        {
            //Se esta haciendo otro constructor para mandar llamar los datos que se estan
            //agregando y guardando los mande al datagrid automaticamente

            _lpersona = new LPersona();
            _etipopersonal = new ETipoPersonal();


        }
        private void BindingEtipoPersonal()
        {
            if (_isEnableBinding)
            {
                
                

                //_etipopersonal.IdTIPOPERSONAL = int.Parse(textBox1.Text);
                _etipopersonal.TIPOPERSONAL = textBox2.Text;

            }

        }
        private void BindingEtipoPersonal2()
        {
            if (_isEnableBinding)
            {



               _etipopersonal.IdTIPOPERSONAL = int.Parse(textBox1.Text);
                _etipopersonal.TIPOPERSONAL = textBox2.Text;

            }

        }


        private void buscarTipoPersonal(string filtro)
        {
            dtg_Datos.DataSource = _lpersona.ObtenerLista(filtro);
        }

        private void Tipo_de_Personal_Load(object sender, EventArgs e)
        {
            buscarTipoPersonal("");
        }

        private void btn_Nuevo_Click(object sender, EventArgs e)
        {

            BindingEtipoPersonal();
            buscarTipoPersonal("");


            Guardar();
        }

        private void Guardar()

        {
            _lpersona.Guardar(_etipopersonal);


        }
        

        private void btn_Eliminar_Click(object sender, EventArgs e)
        {

            
            if (MessageBox.Show("estas seguro que deseas eliminar este registro?", "Eliminar registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    Eliminar();
                    buscarTipoPersonal("");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void Eliminar()

        {
            _lpersona.Eliminar(_etipopersonal);


        }

        private void button1_Click(object sender, EventArgs e)
        {
            BindingEtipoPersonal2();
            buscarTipoPersonal("");



            Actualizar();
        }
        private void Actualizar()

        {
            _lpersona.Actualizar(_etipopersonal);


        }

    }
}

   

