﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.RegistroAdministraccion
{
  public   class Checadas
    {

        private int _Idchecada;
        private string _Fecha;
        private string _Hora;
        private string _fkRFC;

        public int Idchecada { get => _Idchecada; set => _Idchecada = value; }
        public string Fecha { get => _Fecha; set => _Fecha = value; }
        public string Hora{ get => _Hora; set => _Hora = value; }
       public string fkRFC { get => _fkRFC; set => _fkRFC = value; }
    }
}
