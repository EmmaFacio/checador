﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.RegistroAdministraccion
{
   public  class ETipoPersonal
    {

        private int _IdTIPOPERSONAL;
        private string _TIPOPERSONAL;

        public int IdTIPOPERSONAL { get => _IdTIPOPERSONAL; set => _IdTIPOPERSONAL = value; }
        public string TIPOPERSONAL { get => _TIPOPERSONAL; set => _TIPOPERSONAL = value; }
    }
}
