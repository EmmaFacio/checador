﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.RegistroAdministraccion
{
   public  class Horarios
    {
        private int _id;
        private string _diastrabajados;
        private string _horarioentrada;
        private string _horariosalida;
        private int _fkRFC;

        public int Id { get => _id; set => _id = value; }
        public string Diastrabajados { get => _diastrabajados; set => _diastrabajados = value; }
        public string Horarioentrada { get => _horarioentrada; set => _horarioentrada = value; }
        public string Horariosalida { get => _horariosalida; set => _horariosalida = value; }
        public int FkRFC { get => _fkRFC; set => _fkRFC = value; }
    }
}
