﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.RegistroAdministraccion;
using Conexionbd;
using System.Data;

namespace AccesoDatos.RegistroAdministraccion
{
    public class HorariosAccesoDatos
    {

        Conexion _conexion;
        public HorariosAccesoDatos()
        {
            _conexion = new Conexion("localhost", "root", "", "checador", 3306);

        }

        //metodos guardar eliminar y actualizar .
        public void Eliminar(Horarios id)
        {
            string cadena = string.Format("Delete from horarios where id={0}", id);
            _conexion.EjecutarConsulta(cadena);
        }
        public void Guardar(Horarios horarios)
        {
            if (horarios.Id == 0)
            {
                string cadena = string.Format("Insert into horarios values('{0}','{1}','{2}','{3}')", horarios.Diastrabajados, horarios.Horarioentrada, horarios.Horariosalida, horarios.FkRFC);
                _conexion.EjecutarConsulta(cadena);
            }
        }
        public void actualizar(Horarios horarios)

        {
            string cadena = string.Format("Update horarios set diastrabajados = '{0}', horarioentrada = '{1}', horariosalida = '{2}' where fkRFC = '{3}'", horarios.Diastrabajados, horarios.Horarioentrada, horarios.Horariosalida, horarios.FkRFC);
            _conexion.EjecutarConsulta(cadena);
        }
        public List<Horarios> ObtenerLista(string filtro)
        {
            var list = new List<Horarios>();
            string consulta = string.Format("Select * from horarios", filtro);
            var ds = _conexion.ObtenerDatos(consulta, "horarios");
            var dt = ds.Tables[0]; //Variable data table arreglo de tablas

            foreach (DataRow row in dt.Rows)
            {
                var ti = new Horarios
                {
                   Id= int.Parse(row["Id"].ToString()),
                   Diastrabajados  = row["diastrabajados"].ToString(),
                    Horarioentrada = row["horarioentrada"].ToString(),
                    Horariosalida= row["horariosalida"].ToString(),
                    FkRFC = int.Parse(row["Id"].ToString()),
                };

                list.Add(ti);
            }


            return list;
        }

    }
}


