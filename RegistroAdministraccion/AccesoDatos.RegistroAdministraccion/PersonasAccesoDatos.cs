﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.RegistroAdministraccion;
using Conexionbd;
using System.Data;

namespace AccesoDatos.RegistroAdministraccion
{
    public class PersonasAccesoDatos
    {
        Conexion _conexion;
        public PersonasAccesoDatos()
        {
            _conexion = new Conexion("localhost", "root", "", "checador", 3306);

        }
        //metodos guardar eliminar y actualizar .
        public void Eliminar(BPersonal bPersonal)
        {
            string cadena = string.Format("Delete from personal where RFC={0}", bPersonal.RFC);
            _conexion.EjecutarConsulta(cadena);
        }
        public void Guardar(BPersonal bPersonal)
        {
            if (bPersonal.Nombre!= "")
            {
                string cadena = string.Format("Insert into personal values('{0}','{1}','{2}','{3}','{4}')", bPersonal.RFC,bPersonal.Nombre,bPersonal.ApellidoP,bPersonal.ApellidoP,bPersonal.FechaNacimeinto);
                _conexion.EjecutarConsulta(cadena);
            }
        }
        public void actualizar(BPersonal bPersonal)

        {
            string cadena = string.Format("Update personal set Nombre = '{0}', ApellidoP = '{1}', ApellidoM = '{2}',FechaNacimiento='{3}'  where RFC = '{4}'", bPersonal.Nombre, bPersonal.ApellidoP, bPersonal.ApellidoM, bPersonal.FechaNacimeinto,bPersonal.RFC);
            _conexion.EjecutarConsulta(cadena);
        }
    
public List<BPersonal> ObtenerLista(string filtro)
{
    var list = new List<BPersonal>();
    string consulta = string.Format("Select * from personal", filtro);
    var ds = _conexion.ObtenerDatos(consulta, "personal");
    var dt = ds.Tables[0]; //Variable data table arreglo de tablas

    foreach (DataRow row in dt.Rows)
    {
        var tipo = new BPersonal
        {
            RFC = int.Parse(row["RFC"].ToString()),
            Nombre = row["nombre"].ToString(),
            ApellidoP = row["ApellidoP"].ToString(),
            ApellidoM= row["ApellidoM"].ToString(),
            FechaNacimeinto= row["FechaNacimeinto"].ToString()
        };

        list.Add(tipo);
    }


    return list;
}

    }
}
   
