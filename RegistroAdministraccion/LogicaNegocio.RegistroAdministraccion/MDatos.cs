﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.RegistroAdministraccion;
using AccesoDatos.RegistroAdministraccion;
//using System.Text.RegularExpressions;

namespace LogicaNegocio.RegistroAdministraccion
{
    public class MDatos
    {
        //mandandodo llamar metodo de acceso a datos 
        private PersonasAccesoDatos _PersonasAccesoDatos;
        public MDatos()
        {
            _PersonasAccesoDatos = new PersonasAccesoDatos();
        }
        public void Eliminar(BPersonal bPersonal)
        {
            _PersonasAccesoDatos.Eliminar(bPersonal);
        }
        public void Guardar(BPersonal bPersonal)
        {
            _PersonasAccesoDatos.Guardar(bPersonal);
        }
        public void Actualizar(BPersonal bPersonal)
        {
            _PersonasAccesoDatos.actualizar(bPersonal);
        }
        public List<BPersonal> ObtenerLista(string filtro)
        {
            var list = new List<BPersonal>();
            list = _PersonasAccesoDatos.ObtenerLista(filtro);
            return list;
        }
    }
}
    
