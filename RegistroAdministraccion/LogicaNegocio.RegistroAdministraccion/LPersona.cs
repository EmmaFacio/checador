﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.RegistroAdministraccion;
using AccesoDatos.RegistroAdministraccion;
using System.Text.RegularExpressions;

namespace LogicaNegocio.RegistroAdministraccion
{
    public class LPersona
    {
        //mandandodo llamar metodo de acceso a datos 
        private  TipoPersonasAccesoDatos _TipopersonasAccesodatos;
        public LPersona()
        {
            _TipopersonasAccesodatos = new TipoPersonasAccesoDatos();
        }
        public void Eliminar(ETipoPersonal eTipoPersonal)
        {
            _TipopersonasAccesodatos.Eliminar(eTipoPersonal);
        }

        public void Guardar(ETipoPersonal eTipoPersonal)
        {
            _TipopersonasAccesodatos.Guardar(eTipoPersonal);
        }
        public void Actualizar(ETipoPersonal eTipoPersonal)
        {
            _TipopersonasAccesodatos.actualizar(eTipoPersonal);
        }


        public List<ETipoPersonal> ObtenerLista(string filtro)
        {
            var list = new List<ETipoPersonal>();
            list = _TipopersonasAccesodatos.ObtenerLista(filtro);
            return list;
        }





    }
}
