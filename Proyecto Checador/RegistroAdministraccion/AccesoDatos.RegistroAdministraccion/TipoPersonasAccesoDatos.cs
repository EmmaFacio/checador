﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.RegistroAdministraccion;
using Conexionbd;
using System.Data;
//using System.Security.Cryptography.X509Certificates;

namespace AccesoDatos.RegistroAdministraccion
{
    public class TipoPersonasAccesoDatos
    {

        Conexion _conexion;
        public TipoPersonasAccesoDatos()
        {
            _conexion = new Conexion("localhost", "root", "", "checador", 3306);

        }
        //metodos guardar eliminar y actualizar .
        public void Eliminar(ETipoPersonal eTipoPersonal)
        {
            string cadena = string.Format("Delete from tipopersonal where IdTIPOPERSONAL={0}", eTipoPersonal.IdTIPOPERSONAL);
            _conexion.EjecutarConsulta(cadena);
        }

        public void Guardar(ETipoPersonal eTipoPersonal)
        {
            if (eTipoPersonal.TIPOPERSONAL != "")
            {
                string cadena = string.Format("Insert into tipopersonal values(null, '{0}')", eTipoPersonal.TIPOPERSONAL);
                _conexion.EjecutarConsulta(cadena);
            }
        }
        public void actualizar(ETipoPersonal eTipoPersonal)

        {
            string cadena = string.Format("Update tipopersonal set TIPOPERSONAL= '{0}' where IdTIPOPERSONAL='{1}'", eTipoPersonal.TIPOPERSONAL, eTipoPersonal.IdTIPOPERSONAL);
            _conexion.EjecutarConsulta(cadena);
        }



        public List<ETipoPersonal> ObtenerLista(string filtro)
        {
            var list = new List<ETipoPersonal>();
            string consulta = string.Format("Select * from tipopersonal", filtro);
            var ds = _conexion.ObtenerDatos(consulta, "tipopersonal");
            var dt = ds.Tables[0]; //Variable data table arreglo de tablas

            foreach (DataRow row in dt.Rows)
            {
                var etipo = new ETipoPersonal
                {
                    IdTIPOPERSONAL = int.Parse(row["IdTIPOPERSONAL"].ToString()),
                    TIPOPERSONAL = row["TIPOPERSONAL"].ToString(),

                };

                list.Add(etipo);
            }


            return list;
        }

    }
}
   